module.exports = ({ env }) => ({
  auth: {
    secret: env('ADMIN_JWT_SECRET', '8a6b3f8f230a364bfb068b422904da55'),
  },
});
