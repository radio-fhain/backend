# Structure

# Types

## First
* __Pages__
  * Home
  * About
  * Artists / Collaborators

* __Components__
  * Stream
  * Player (radio)
  * Programme / Upcoming
  * Banner / Header
  * Newsletter signup
  * Donate
  * About
  * Socials

## Then
* Events
  * Past
  * Upcoming
* Programme

* Chatroom